-module(fib).

-export([fib_p/1]).
-export([fib_g/1]).
-export([tail_fib/1]).

fib_p(0) ->
    0;
fib_p(1) ->
    1;
fib_p(N) ->
    fib_p(N - 1) + fib_p(N - 2).

fib_g(N) when N > 1 ->
    fib_g(N - 1) + fib_g(N - 2);
fib_g(N) ->
	N.

tail_fib(N) ->
	tail_fib_helper(N, 0 ,1).
tail_fib_helper(0, PrevResult, _) ->
	PrevResult;
tail_fib_helper(N, PrevResult, CurResult) ->
	tail_fib_helper(N - 1, CurResult, PrevResult + CurResult).
